﻿using Microsoft.Toolkit.Mvvm.ComponentModel;
using Microsoft.Toolkit.Mvvm.Messaging;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace Rectangles.GUI
{
    public class InputViewModel : ObservableObject
    {
        private readonly IMessenger _messenger;

        public InputViewModel(IMessenger messenger)
        {
            InputLimit = 10;
            _messenger = messenger;
            Inputs = new ObservableCollection<RectangleViewModel>();
            AddCommand = new RelayCommand(AddCommandExecute, AddCommandCanExecute);
            RemoveCommand = new RelayCommand(RemoveCommandExecute, RemoveCommandCanExecute);
            ImportCommand = new RelayCommand(ImportCommandExecute, ImportCommandCanExecute);
        }


        public int InputLimit { get; }
        public ObservableCollection<RectangleViewModel> Inputs { get; }
        public ICommand AddCommand { get; }
        public ICommand RemoveCommand { get; }
        public ICommand ImportCommand { get; }

        private void AddCommandExecute()
        {
            var model = new Rectangle(1, 1);
            var viewModel = new RectangleViewModel(model);
            Inputs.Add(viewModel);
        }

        private bool AddCommandCanExecute() => Inputs.Count < InputLimit;

        private void RemoveCommandExecute()
        {
            var entries = Inputs.Where(x => x.IsSelected).ToArray();
            foreach (var entry in entries)
                Inputs.Remove(entry);
        }

        private bool RemoveCommandCanExecute() => Inputs.Any(x => x.IsSelected);

        private void ImportCommandExecute()
        {
            var validRectangles = Inputs.Where(x => !x.HasErrors).ToArray();
            var message = new RectangleMessage(validRectangles);
            _messenger.Send(message);
            foreach (var rectangle in validRectangles)
                Inputs.Remove(rectangle);
        }

        private bool ImportCommandCanExecute() => Inputs.Any(x => !x.HasErrors);
    }
}
