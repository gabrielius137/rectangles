﻿namespace Rectangles.GUI
{
    public interface ISelected
    {
        bool IsSelected { get; set; }
    }
}