﻿using Microsoft.Toolkit.Mvvm.ComponentModel;
using System;
using System.ComponentModel.DataAnnotations;

namespace Rectangles.GUI
{
    public class RectangleViewModel : ObservableValidator, ISelected
    {
        private readonly Rectangle _rectangle;

        public RectangleViewModel(Rectangle rectangle)
        {
            _rectangle = rectangle;
            ValidateAllProperties();
        }

        [Required]
        [CustomValidation(typeof(RectangleViewModel), nameof(ValidateRectangleSide))]
        public uint Length
        {
            get => _rectangle.Length;
            set
            {
                if (SetProperty(_rectangle.Length, value, _rectangle, (rectangle, length) => rectangle.Length = length, true))
                    Update();
            }
        }

        [Required]
        [CustomValidation(typeof(RectangleViewModel), nameof(ValidateRectangleSide))]
        public uint Width
        {
            get => _rectangle.Width;
            set
            {
                if (SetProperty(_rectangle.Width, value, _rectangle, (rectangle, width) => rectangle.Width = width, true))
                    Update();
            }
        }

        private bool _isSelected;

        public bool IsSelected
        {
            get => _isSelected;
            set => SetProperty(ref _isSelected, value);
        }


        public double Diagonal => Math.Sqrt(Length * Length + Width * Width);
        public uint Area => Length * Width;

        public RectangleType RectangleType
        {
            get
            {
                if (Width > Length)
                    return RectangleType.Tall;
                else if (Width < Length)
                    return RectangleType.Flat;
                else
                    return RectangleType.Square;
            }
        }

        private void Update()
        {
            OnPropertyChanged(nameof(Diagonal));
            OnPropertyChanged(nameof(Area));
            OnPropertyChanged(nameof(RectangleType));
        }

        public static ValidationResult ValidateRectangleSide(uint side)
        {
            if (side < 1)
                return new ValidationResult("Must be greater than 0");

            return ValidationResult.Success;
        }
    }

    public enum RectangleType
    {
        Tall,
        Flat,
        Square
    }
}
