﻿using Microsoft.Toolkit.Mvvm.ComponentModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rectangles.GUI
{
    public class MainWindowViewModel : ObservableObject
    {
        public MainWindowViewModel()
        {
            FontSize = 16;
            WindowName = "Rectangles";
        }

        public int FontSize { get; }
        public string WindowName { get; }
    }
}
