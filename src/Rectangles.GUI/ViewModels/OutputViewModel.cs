﻿using Microsoft.Toolkit.Mvvm.ComponentModel;
using Microsoft.Toolkit.Mvvm.Messaging;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Data;
using System.Windows.Input;

namespace Rectangles.GUI
{
    public class OutputViewModel : ObservableObject, IRecipient<RectangleMessage>
    {
        private readonly SortDescription SortingPredicate = new SortDescription("Diagonal", ListSortDirection.Ascending);
        private readonly CollectionViewSource _outputs;
        private readonly IList<RectangleViewModel> _collection;
        private readonly IMessenger _messenger;

        public OutputViewModel(IMessenger messenger)
        {
            _collection = new List<RectangleViewModel>();
            _outputs = new CollectionViewSource() { Source = _collection };
            _outputs.SortDescriptions.Add(SortingPredicate);
            RemoveCommand = new RelayCommand(RemoveCommandExecute, RemoveCommandCanExecute);
            _messenger = messenger;
            _messenger.Register(this);
        }

        public ICollectionView Outputs => _outputs.View;
        public ICommand RemoveCommand { get; }

        private void RemoveCommandExecute()
        {
            var entries = _collection.Where(x => x.IsSelected).ToArray();
            foreach (var entry in entries)
                _collection.Remove(entry);

            OnPropertyChanged(nameof(SquareRatio));

            // Dependency on the dispatcher
            if (App.Current != null)
                Outputs.Refresh();
        }

        private bool RemoveCommandCanExecute() => _collection.Any(x => x.IsSelected);

        public double SquareRatio =>
            _collection.Count != 0 ?
            ((double)_collection.Count(x => x.RectangleType == RectangleType.Square)) / (_collection.Count) :
            0;

        public void Receive(RectangleMessage message)
        {
            foreach (var rectangle in _collection)
                // existing rectangles are deselected
                rectangle.IsSelected = false;
            foreach (var rectangle in message.Rectangles)
            {
                // added new rectangles are selected
                rectangle.IsSelected = true;
                _collection.Add(rectangle);
            }

            OnPropertyChanged(nameof(SquareRatio));

            // Dependency on the dispatcher
            if (App.Current != null)
                Outputs.Refresh();
        }
    }
}
