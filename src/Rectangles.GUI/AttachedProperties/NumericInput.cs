﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;

namespace Rectangles.GUI
{
    /// <summary>
    /// Attached property for handling numeric (in this case whole numbers)
    /// input in text box controls
    /// </summary>
    public static class NumericInput
    {
        private const string NULL_FALLBACK = "0";

        public static readonly DependencyProperty IsEnabledProperty =
            DependencyProperty.RegisterAttached("IsEnabled", typeof(bool),
                typeof(NumericInput), new UIPropertyMetadata(false, OnValueChanged));

        public static bool GetIsEnabled(Control control) => (bool)control.GetValue(IsEnabledProperty);

        public static void SetIsEnabled(Control control, bool value) => control.SetValue(IsEnabledProperty, value);


        private static void OnValueChanged(DependencyObject dependencyObject,
                                            DependencyPropertyChangedEventArgs e)
        {
            var control = dependencyObject as TextBox;
            if (control == null) return;
            if (e.NewValue is bool && (bool)e.NewValue)
            {
                control.PreviewTextInput += OnTextInput;
                control.PreviewKeyDown += OnPreviewKeyDown;
                DataObject.AddPastingHandler(control, OnPaste);
            }
            else
            {
                control.PreviewTextInput -= OnTextInput;
                control.PreviewKeyDown -= OnPreviewKeyDown;
                DataObject.RemovePastingHandler(control, OnPaste);
            }
        }

        /// <summary>
        /// Prevent input with non numeric chars
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">input event</param>
        private static void OnTextInput(object sender, TextCompositionEventArgs e)
        {
            if (e.Text.Any(c => !char.IsDigit(c)))
                e.Handled = true;
        }

        /// <summary>
        /// Prevent input with non numeric chars
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">input event</param>
        private static void OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            var textbox = sender as TextBox;
            if (textbox == null) return;
            switch (e.Key)
            {
                // if backspace with carret in front of a single char text
                // or selected text is the same as current text
                case Key.Back:
                    if ((textbox.Text.Length == 1 && textbox.CaretIndex == 1)
                        || textbox.Text == textbox.SelectedText)
                    {
                        Resolve(textbox, e);
                    }
                    break;
                // if delete with carret in back of a single char text
                // or selected text is the same as current text
                case Key.Delete:
                    if ((textbox.Text.Length == 1 && textbox.CaretIndex == 0)
                        || textbox.Text == textbox.SelectedText)
                    {
                        Resolve(textbox, e);
                    }
                    break;
                // whitespace is unnecessary
                case Key.Space:
                    e.Handled = true;
                    break;
            }
        }

        private static void Resolve(TextBox textbox, KeyEventArgs e)
        {
            textbox.Text = NULL_FALLBACK;
            textbox.CaretIndex = NULL_FALLBACK.Length;
            e.Handled = true;
        }

        /// <summary>
        /// Prevent paste input with non numeric chars
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">paste input event</param>
        private static void OnPaste(object sender, DataObjectPastingEventArgs e)
        {
            if (e.DataObject.GetDataPresent(DataFormats.Text))
            {
                var text = Convert.ToString(e.DataObject.GetData(DataFormats.Text)).Trim();
                if (text.Any(c => !char.IsDigit(c)))
                    e.CancelCommand();
            }
            else
            {
                e.CancelCommand();
            }
        }
    }
}
