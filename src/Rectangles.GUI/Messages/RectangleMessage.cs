﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rectangles.GUI
{
    public class RectangleMessage
    {
        public RectangleMessage(IEnumerable<RectangleViewModel> rectangles)
        {
            Rectangles = rectangles ?? throw new ArgumentNullException(nameof(rectangles));
        }

        public IEnumerable<RectangleViewModel> Rectangles { get; private set; }
    }
}
