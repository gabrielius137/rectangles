﻿namespace Rectangles.GUI
{
    public class Rectangle
    {
        public Rectangle(uint length, uint width)
        {
            Length = length;
            Width = width;
        }

        public uint Length { get; set; }
        public uint Width { get; set; }
    }
}
