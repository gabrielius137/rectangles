﻿using System.Windows.Controls;

namespace Rectangles.GUI
{
    /// <summary>
    /// Interaction logic for OutputView.xaml
    /// </summary>
    public partial class OutputView : UserControl
    {
        public OutputView()
        {
            InitializeComponent();
            DataContext = App.Current.Services.GetService(typeof(OutputViewModel));
        }
    }
}
