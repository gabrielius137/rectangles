﻿using System.Windows.Controls;

namespace Rectangles.GUI
{
    /// <summary>
    /// Interaction logic for InputView.xaml
    /// </summary>
    public partial class InputView : UserControl
    {
        public InputView()
        {
            InitializeComponent();
            DataContext = App.Current.Services.GetService(typeof(InputViewModel));
        }
    }
}
