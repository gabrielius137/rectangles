﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Markup;

namespace Rectangles.GUI
{
    /// <summary>
    /// Base value converter that allows direct XAML usage
    /// </summary>
    /// <typeparam name="TConverter">The type of this value converter</typeparam>
    public abstract class BaseValueConverter<TConverter> : MarkupExtension, IValueConverter
        where TConverter : class, new()
    {
        /// <summary>
        /// A single static instance of this value converter
        /// </summary>
        private static TConverter _converter;

        /// <summary>
        /// Provides static instance of the value converter
        /// </summary>
        /// <param name="serviceProvider">The service provider</param>
        /// <returns>Instance of converter</returns>
        public override object ProvideValue(IServiceProvider serviceProvider)
            => _converter ??= new TConverter();

        public abstract object Convert(object value, Type targetType, object parameter, CultureInfo culture);

        public abstract object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture);
    }
}
