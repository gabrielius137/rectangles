﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Rectangles.GUI
{
    public class ErrorCollectionConverter : BaseValueConverter<ErrorCollectionConverter>
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return null;
            var errors = (ReadOnlyObservableCollection<ValidationError>)value;
            var stringErrors = new List<string>();

            foreach (var error in errors)
                stringErrors.Add(error.ErrorContent.ToString());
            
            return new ListBox
            {
                ItemsSource = stringErrors,
                BorderThickness = new Thickness(0),
                Background = Brushes.Transparent
            };
        }

        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
