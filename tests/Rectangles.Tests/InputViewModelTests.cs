﻿using Rectangles.GUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Rectangles.Tests
{
    public class InputViewModelTests
    {
        private readonly InputViewModel _inputViewModel;

        public InputViewModelTests()
        {
            _inputViewModel = new InputViewModel(null);
        }

        [Fact]
        public void AddingNewInput()
        {
            var before = _inputViewModel.Inputs.Count;
            _inputViewModel.AddCommand.Execute(null);
            var after = _inputViewModel.Inputs.Count;

            Assert.True(before + 1 == after, "Input count must increase by one");
        }

        [Fact]
        public void InputThresholdExists()
        {
            for (int i = 0; i < _inputViewModel.InputLimit; i++)
                _inputViewModel.AddCommand.Execute(null);

            var before = _inputViewModel.Inputs.Count;
            _inputViewModel.AddCommand.Execute(null);
            var after = _inputViewModel.Inputs.Count;

            Assert.True(before == after, "Input count must not change after reaching threshold");
        }

        [Fact]
        public void RemoveOnlySelectedElements()
        {
            var data = TestData.Data;
            foreach (var item in data)
                _inputViewModel.Inputs.Add(item);

            var before = data.Take(2).ToHashSet();

            foreach (var item in before)
                item.IsSelected = true;

            _inputViewModel.RemoveCommand.Execute(null);

            Assert.False(before.Overlaps(_inputViewModel.Inputs), "Collections before and after removal must not overlap");
        }
    }
}
