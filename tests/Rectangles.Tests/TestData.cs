﻿using Rectangles.GUI;
using System.Collections.Generic;

namespace Rectangles.Tests
{
    public static class TestData
    {
        public static List<RectangleViewModel> Data =>
            new List<RectangleViewModel>()
            {
                new RectangleViewModel(new Rectangle(1, 1)),
                new RectangleViewModel(new Rectangle(0, 0)),
                new RectangleViewModel(new Rectangle(5, 0)),
                new RectangleViewModel(new Rectangle(2, 3)),
                new RectangleViewModel(new Rectangle(3, 2))
            };
    }
}
