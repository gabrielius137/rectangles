﻿using Rectangles.GUI;
using Xunit;

namespace Rectangles.Tests
{
    public class RectangleViewModelTests
    {
        [Theory]
        [InlineData(3, 3, RectangleType.Square)]
        [InlineData(2, 4, RectangleType.Tall)]
        [InlineData(4, 2, RectangleType.Flat)]
        public void RectangleTypeIsDeterminedProperly(uint width, uint length, RectangleType type)
        {
            var rectangle = new Rectangle(width, length);
            var viewModel = new RectangleViewModel(rectangle);
            Assert.True(viewModel.RectangleType == type, "Rectangle types must match");
        }

        [Fact]
        public void RectangleValidatesAsExpected()
        {
            var rectangle = new Rectangle(1, 1);
            var viewModel = new RectangleViewModel(rectangle);
            Assert.False(viewModel.HasErrors, "Valid rectangle expected");

            viewModel.Width = 0;
            Assert.True(viewModel.HasErrors, "Invalid rectangle expected");

            viewModel.Width = 5;
            Assert.False(viewModel.HasErrors, "Valid rectangle expected");
        }
    }
}
