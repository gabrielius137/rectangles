using Microsoft.Toolkit.Mvvm.Messaging;
using Moq;
using Rectangles.GUI;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;
using System.Xml.Linq;

namespace Rectangles.Tests
{
    public class OutputViewModelTests
    {
        private readonly OutputViewModel _viewModel;

        public OutputViewModelTests()
        {
            _viewModel = new OutputViewModel(WeakReferenceMessenger.Default);
        }

        [Fact]
        public void RemoveOnlySelectedElements()
        {
            var data = TestData.Data;
            var message = new RectangleMessage(TestData.Data);
            _viewModel.Receive(message);

            // elements which are going to be deleted
            var before = _viewModel.Outputs.SourceCollection
                            .Cast<RectangleViewModel>()
                            .Where(x => x.IsSelected)
                            .ToHashSet();

            _viewModel.RemoveCommand.Execute(null);

            var after = _viewModel.Outputs.SourceCollection
                            .Cast<RectangleViewModel>()
                            .ToArray();

            Assert.False(before.Overlaps(after), "Collections before and after removal must not overlap");
        }

        [Fact]
        public void NoDivisionByZeroErrorAndExpectedRatio()
        {
            var result = _viewModel.SquareRatio;

            Assert.True(result == 0, "While output collection is empty ratio must be equal to 0");

            var data = TestData.Data.Where(x => !x.HasErrors).ToArray();
            var squareCount = data.Count(x => x.RectangleType == RectangleType.Square);

            _viewModel.Receive(new RectangleMessage(data));

            result = (double)squareCount / data.Length;
            Assert.True(result == _viewModel.SquareRatio, "Ratios must be equal");
        }
    }
}
