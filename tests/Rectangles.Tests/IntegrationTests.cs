﻿using Microsoft.Toolkit.Mvvm.Messaging;
using Rectangles.GUI;
using System.Linq;
using Xunit;

namespace Rectangles.Tests
{
    public class IntegrationTests
    {
        private readonly InputViewModel _inputViewModel;
        private readonly OutputViewModel _outputViewModel;

        public IntegrationTests()
        {
            var messenger = WeakReferenceMessenger.Default;
            _inputViewModel = new InputViewModel(messenger);
            _outputViewModel = new OutputViewModel(messenger);
        }

        [Fact]
        public void OnlyValidViewModelsAreImported()
        {
            var data = TestData.Data;
            foreach (var input in data)
                _inputViewModel.Inputs.Add(input);

            var before = data.Where(x => !x.HasErrors).ToHashSet();
            _inputViewModel.ImportCommand.Execute(null);
            var after = _outputViewModel.Outputs.SourceCollection
                            .Cast<RectangleViewModel>()
                            .ToHashSet();

            Assert.True(before.SetEquals(after), "Only valid inputs must be recieved/sent");
        }
    }
}
